class AddNameToReports < ActiveRecord::Migration
  def change
    add_column :reports, :name, :text
  end
end
