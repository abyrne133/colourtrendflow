class AddManufacturingToUsers < ActiveRecord::Migration
  def change
    add_column :users, :manufacturing, :boolean
  end
end
