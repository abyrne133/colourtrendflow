class AddQcToUsers < ActiveRecord::Migration
  def change
    add_column :users, :qc, :boolean
  end
end
