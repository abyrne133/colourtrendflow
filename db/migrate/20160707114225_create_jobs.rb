class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.integer :no
      t.string :desc
      t.integer :size
      t.string :make
      t.text :makecomment
      t.string :qc
      t.text :qccomment
      t.string :fill
      t.text :fillcomment

      t.timestamps null: false
    end
  end
end
