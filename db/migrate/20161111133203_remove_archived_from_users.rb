class RemoveArchivedFromUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :archived, :boolean
  end
end
