class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@colourtrend.ie"
  layout 'mailer'
end
