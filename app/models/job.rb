class Job < ApplicationRecord
     
     before_save :default_values
     def default_values
          self.make ||= 'Not Started'
          self.qc ||= 'Not Started'
          self.fill ||= 'Not Started'
     end
     
     default_scope -> { order(created_at: :desc) }
     validates :no,  presence: true, length: { minimum: 5 }, uniqueness: true
     validates :desc,  presence: true, length: { maximum: 30 }
     validates :size,  presence: true
     validates :make,  length: { maximum: 50 }
     validates :qc,   length: { maximum: 50 }
     validates :fill,   length: { maximum: 50 }
end
