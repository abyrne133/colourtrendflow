class Report < ApplicationRecord
     attr_accessor :image
     default_scope -> { order(created_at: :desc) }
     validates :name,  presence: true, length: { minimum: 1 }
     validates :image,  presence: true
     mount_uploader :image, ImageUploader
end
