App.messages = App.cable.subscriptions.create('MessagesChannel', {  
  received: function(data) {
    if(window.location.href.toUpperCase().indexOf("CHATROOMS/CHAT") === -1) {
      $('#notification').text("!");
    }
     
    
    $("#messages").removeClass('hidden')
    document.getElementById('clearable').value = "";
    $('#messages').append(this.renderMessage(data));
     var objDiv = document.getElementById("messages");
      return objDiv.scrollTop = objDiv.scrollHeight;
  },
  renderMessage: function(data) {
    return "<p> <b>" + data.user + ": </b>" + data.message + "</p>";
  }
});