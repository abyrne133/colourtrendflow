class ReportsController < ApplicationController
  before_filter :check_for_mobile

  def index
    if logged_in? then
      if executive? then
        @reports = Report.paginate(page: params[:page])
        render '/reports/index'
      else
        flash[:warning] = "Reporting Access Denied"
        redirect_to jobs_url
      end
    else
      render '/sessions/new'
    end
  end
  
  def show
     @report = Report.find(params[:id])
  end

  def new
    @report = Report.new
  end

  def edit
    @report = Report.find(params[:id])
  end

  def create
    @report = Report.new(report_params)
    if @report.save
      flash[:success] = "Report successfully added!"
       redirect_to reports_url
    else
      render 'new'
    end
  end

  def update
    @report = Report.find(params[:id])
    if @report.update_attributes(report_params)
      flash[:success] = "Report updated"
       redirect_to reports_url
    else
      render 'edit'
    end
  end

  def destroy
    Report.find(params[:id]).destroy
    flash[:success] = "Report deleted"
     redirect_to reports_url
  end

  private
    def report_params
      params.require(:report).permit(:name, :image)
    end
end