class ChatroomsController < ApplicationController
   before_filter :check_for_mobile
  def index
    @chatroom = Chatroom.new
    @chatrooms = Chatroom.all
  end

  def new
    if request.referrer.split("/").last == "chatrooms"
      flash[:notice] = nil
    end
    @chatroom = Chatroom.new
  end

  def edit
    @chatroom = Chatroom.find_by(slug: params[:slug])
  end

  def create
    @chatroom = Chatroom.new(chatroom_params)
    if @chatroom.save
      respond_to do |format|
        format.html { redirect_to @chatroom }
        format.js
      end
    else
      respond_to do |format|
        flash[:notice] = {error: ["a chatroom with this topic already exists"]}
        format.html { redirect_to new_chatroom_path }
        format.js { render template: 'chatrooms/chatroom_error.js.erb'} 
      end
    end
  end

  def update
    chatroom = Chatroom.find_by(slug: params[:slug])
    chatroom.update(chatroom_params)
    redirect_to chatroom
  end

  def show
    if logged_in? then
      @chatroom = Chatroom.find_by topic: 'Chat'
      @message = Message.new
    else
      render '/sessions/new'
    end
  end
  
  def destroy #actually clears all messages, not a chatroom as only one chatroom will ever be used.
    Message.delete_all
    flash[:success] = "Messages cleared"
    redirect_to chatroom_path('Chat')
  end

  private

    def chatroom_params
      params.require(:chatroom).permit(:topic)
    end
end
