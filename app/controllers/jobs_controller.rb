class JobsController < ApplicationController
  before_filter :check_for_mobile
  def index
    if logged_in? then
      @jobs = Job.where(archived: false).paginate(page: params[:page])

      render '/jobs/index'
    else
      render '/sessions/new'
    end
  end
  
  def archiveindex
    if logged_in? then
      @jobs = Job.where(archived: true).paginate(page: params[:page])
      render '/jobs/index'
    else
      render '/sessions/new'
    end
  end
  
  def show
    @job = Job.find(params[:id])
  end
  
  def new
    @job = Job.new
  end
  
  def create
    @job = Job.new(job_params)
    if @job.save
      flash[:success] = "Job successfully added!"
       redirect_to root_path
    else
      render 'new'
    end
  end
  
  def update
    @job = Job.find(params[:id])
    if @job.update_attributes(job_params)
      flash[:success] = "Job updated"
       redirect_to root_path
    else
      render 'edit'
    end
  end
  
  def edit
    @job = Job.find(params[:id])
  end
  
  def destroy
    Job.find(params[:id]).destroy
    flash[:success] = "Job deleted"
     redirect_to root_path
  end
  
  def archive
    Job.find(params[:id]).update_attributes(:archived => true)
    flash[:success] = "Job successfully archived!"
    redirect_to root_path
  end
  
  def unarchive
    Job.find(params[:id]).update_attributes(:archived => false)
    flash[:success] = "Job successfully un-archived!"
    redirect_to root_path
  end
  
  private

    def job_params
      params.require(:job).permit(:no, :desc, :size,
                                   :make, :makecomment, :qc, :qccomment, :fill, :fillcomment)
    end
    
    
    
    
end