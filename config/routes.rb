Rails.application.routes.draw do
  # Serve websocket cable requests in-process
  mount ActionCable.server => '/cable'

  resources :chatrooms, param: :slug
  resources :messages

  resources :reports
  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'
  get 'signin', to: 'main_pages#signin'
  get '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  
  resources :users
  resources :jobs
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  post 'jobs/archive/:id' => 'jobs#archive', :as => :archive
  post 'jobs/unarchive/:id' => 'jobs#unarchive', :as => :unarchive
  get 'archive' => 'jobs#archiveindex', :as => :archiveindex
  root 'jobs#index'
  
end
